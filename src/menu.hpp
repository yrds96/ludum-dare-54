#ifndef MENU_HPP_INCLUDED
#define MENU_HPP_INCLUDED

struct Menu {
  Menu();
  void update();
  void draw();
};

#endif //MENU_HPP_INCLUDED
