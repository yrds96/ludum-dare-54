#ifndef STRUCTS_HPP_INCLUDED
#define STRUCTS_HPP_INCLUDED

#include "raylib.h"

struct Circle {
  float radius;
};

struct Velocity : Vector2 {};

struct Position : Vector2 {
  Vector2 operator+(const Vector2 &vel) {
    return Vector2{this->x + vel.x, this->y + vel.y};
  }
};

#endif //STRUCTS_HPP_INCLUDED
