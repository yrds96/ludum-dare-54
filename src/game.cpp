#include <string>
#include <vector>

#include "globals.hpp"
#include "structs.hpp"
#include "raylib.h"
#define RAYMATH_IMPLEMENTATION
#include "raymath.h"

#include "game.hpp"

struct Player {
  Position position;
  Circle circle;
  Velocity velocity;
  float speed = 2.0f;
  bool gotHit = false;
  float canPlaceBomb = 1.0f;
  float exp = 0.0f;
};

struct Explosion {
  Position position;
  Circle circle;
  float maxRadius;
};

struct Bomb {
  Position position;
  Circle circle;
  int framesToExplode = 60;
};

Player player {
};
float spawnDistanceFromCenter = 100.0f;
std::vector<Player> enemies;
std::vector<Explosion> explosions;
std::vector<Bomb> bombs;
int framesToEnemySpawn{120};
std::string debugText;
bool gameOver = true;

void createExplosion(Position &pos, float maxRadius = 50) {
  explosions.push_back(Explosion{pos, 10, maxRadius});
}

void createBomb(Position &pos) {
  player.canPlaceBomb = 0.0f;
  bombs.push_back(Bomb{pos, 10, 120});
}

void spawnRandomEnemy() {
  float randomDegrees = GetRandomValue(0, 360);

  const auto enemySize = 10;

  Vector2 origin = {width / 2.0f, height / 2.0f};

  // TODO this is not wrong but is not exactly the right math lol
  //
  auto newEnemy =
      Player{Vector2Add(Vector2Rotate(Vector2{spawnDistanceFromCenter, 0.0f},
                                      randomDegrees * (PI / 180)),
                        origin),
             enemySize, Vector2{1, 1}};

  newEnemy.exp = 10.0f;

  enemies.push_back(newEnemy);
}

void moveEnemiesTowardsPlayer() {
  for (auto &enemy : enemies) {
    const auto diff = Vector2Subtract(player.position, enemy.position);

    const auto diffValue = (diff.x * diff.x) + (diff.y * diff.y);

    const auto distance = sqrtf(diffValue);

    enemy.velocity =
        Velocity{Vector2Normalize({diff.x / distance, diff.y / distance})};

    enemy.position = Position{enemy.position + enemy.velocity};
  }
}

void detectIfEnemiesGotHit() {
  for (auto &explosion : explosions) {
    for (auto &enemy : enemies) {
      if (CheckCollisionCircles(enemy.position, enemy.circle.radius,
                                explosion.position, explosion.circle.radius)) {
        enemy.gotHit = true;
      }
    }
  }
}

void detectIfPlayerWasHit() {
  for (auto &enemy : enemies) {
    if (CheckCollisionCircles(player.position, player.circle.radius,
                              enemy.position, enemy.circle.radius)) {
      player.gotHit = true;
      return;
    };
  }
}

void removeHitEnemies() {
  for (std::vector<Player>::iterator it = enemies.begin();
       it != enemies.end();) {
    if ((*it).gotHit) {
      player.exp += (*it).exp;
      enemies.erase(it);
    } else {
      ++it;
    }
  }
}

void detectGameOver() {
  for (std::vector<Player>::iterator it = enemies.begin();
       it != enemies.end();) {
    if ((*it).gotHit) {
      gameOver = true;
      enemies.erase(it);
    } else {
      ++it;
    }
  }
}

Game::Game() {
  player = {Position{width / 2.0f, height / 2.0f}, Circle{.radius = 25.0f}};
}

void Game::update() {
  static auto framesToSpawn = 120;
  framesToSpawn--;

  if (framesToSpawn == 0) {
    spawnRandomEnemy();
    framesToSpawn = 120;
  }

  // TODO MOVE this to player
  constexpr auto FPS = 60.0f;

  const auto bombSpawnRate = FPS / 60.0f / 60;

  if (player.canPlaceBomb < 1.0f) {
    player.canPlaceBomb += bombSpawnRate;
  }

  player.velocity = {0, 0};

  if (IsKeyDown(KEY_DOWN)) {
    player.velocity.y += 1;
  }

  if (IsKeyDown(KEY_UP)) {
    player.velocity.y -= 1;
  }

  if (IsKeyDown(KEY_LEFT)) {
    player.velocity.x -= 1;
  }

  if (IsKeyDown(KEY_RIGHT)) {
    player.velocity.x += 1;
  }

  if (IsKeyPressed(KEY_SPACE) && player.canPlaceBomb >= 1.0f) {
    createBomb(player.position);
  }

  for (auto &bomb : bombs) {
    bomb.framesToExplode--;
  }

  for (auto &explosion : explosions) {
    explosion.circle.radius = explosion.circle.radius + 2;
  }

  for (std::vector<Bomb>::iterator it = bombs.begin(); it != bombs.end();) {
    if ((*it).framesToExplode == 0) {
      createExplosion((*it).position);
      bombs.erase(it);
    } else {
      ++it;
    }
  }

  for (std::vector<Explosion>::iterator it = explosions.begin();
       it != explosions.end();) {
    if ((*it).circle.radius == (*it).maxRadius) {
      explosions.erase(it);
    } else {
      ++it;
    }
  }

  player.velocity = Velocity{Vector2Normalize(player.velocity)};

  player.velocity.x *= player.speed;
  player.velocity.y *= player.speed;

  player.position = {player.position + player.velocity};

  struct {
    Vector2 position = GetMousePosition();
  } mouse;

  moveEnemiesTowardsPlayer();

  // Detect collision
  //
  detectIfEnemiesGotHit();

  detectIfPlayerWasHit();

  // Kill hit enemies
  removeHitEnemies();

  auto mouseAnglePlayer = Vector2Angle(player.position, mouse.position);

  debugText.clear();

  debugText.append("Mouse Position: ")
      .append(std::to_string(mouse.position.x) + ",")
      .append(std::to_string(mouse.position.y))
      .append("\nPlayer Position: ")
      .append(std::to_string(player.position.x) + ",")
      .append(std::to_string(player.position.y))
      .append("\nMouse Angle(Player): ")
      .append(std::to_string(mouseAnglePlayer))
      .append("\nEnemies: ")
      .append(std::to_string(enemies.size()))
      .append("\nPlayer EXP.")
      .append(std::to_string(player.exp));
}

void Game::draw() {
  struct DrawState {
    DrawState() {
      ClearBackground(RAYWHITE);
      BeginDrawing();
    }
    ~DrawState() { EndDrawing(); }
  } drawState;

  DrawText(debugText.c_str(), 10, 10, 20, RED);

  DrawCircleLines(width / 2.0f, height / 2.0f, spawnDistanceFromCenter, RED);

  for (auto &bomb : bombs) {
    DrawCircle(bomb.position.x, bomb.position.y, bomb.circle.radius, RED);
  }

  for (auto &explosion : explosions) {
    DrawCircle(explosion.position.x, explosion.position.y,
               explosion.circle.radius,
               Color{255, 0, 0,
                     static_cast<unsigned char>(explosion.circle.radius /
                                                explosion.maxRadius * -255)});
  }

  // Draw Player
  DrawCircle(player.position.x, player.position.y, player.circle.radius, BLUE);

  const auto circleColor = player.canPlaceBomb >= 1.0 ? Color{255, 0, 0, 255}
                                                      : Color{255, 0, 0, 128};
  DrawCircle(player.position.x, player.position.y,
             player.circle.radius * player.canPlaceBomb, circleColor);
  DrawRing(player.position, player.circle.radius / 4.0f,
           player.circle.radius / 2.0f, 0.0f, player.exp, 360, WHITE);

  for (const auto &enemy : enemies) {
    DrawCircle(enemy.position.x, enemy.position.y, enemy.circle.radius, BLUE);
    DrawLine(enemy.position.x, enemy.position.y,
             (enemy.position.x + enemy.velocity.x * 20),
             (enemy.position.y + enemy.velocity.y * 20), GREEN);
  }
}
