#include <cmath>
#include <string>
#include <vector>

#include "globals.hpp"
#include "structs.hpp"

#include "game.hpp"
#include "menu.hpp"

enum { GAME, MENU } gameScreen = GAME;

int main(int argc, char *argv[]) {
  struct WindowState {
    WindowState() {
      InitWindow(width, height, "Ludum Dare 55");
      SetTargetFPS(60);
    }
    ~WindowState() { CloseWindow(); }
  } windowState;

  Game game;
  Menu menu;

  while (!WindowShouldClose()) {
    switch (gameScreen) {
    case GAME:
      game.update();
      game.draw();
      break;
    case MENU:
      menu.update();
      menu.draw();
      break;
    }
  }

  return 0;
}
