#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

struct Game {
  Game();
  void update();
  void draw();
};

#endif //GAME_HPP_INCLUDED
